/************************************************************
 * @file ParentLinkDecoratorAlg.cxx
 * @brief Algorithm to add parent link to jet collection that points to itself.
 * **********************************************************/

#include "ParentLinkDecoratorAlg.h"
#include "xAODJet/JetContainer.h"
#include "StoreGate/WriteDecorHandle.h"
#include "StoreGate/ReadDecorHandle.h"


ParentLinkDecoratorAlg::ParentLinkDecoratorAlg(
  const std::string& name, ISvcLocator* loc )
  : AthReentrantAlgorithm(name, loc) {}

StatusCode ParentLinkDecoratorAlg::initialize() {
  ATH_MSG_DEBUG( "Inizializing " << name() << "... " );

  ATH_CHECK( m_JetContainerKey.initialize() );
  m_new_link = m_JetContainerKey.key() + "." + m_new_link.key();
  ATH_CHECK( m_new_link.initialize() );
  
  return StatusCode::SUCCESS;
}

StatusCode ParentLinkDecoratorAlg::execute(const EventContext& ctx) const {
  ATH_MSG_DEBUG( "Executing " << name() << "... " );

  // retrieve jet collection
  SG::ReadHandle<xAOD::JetContainer> jets(m_JetContainerKey, ctx);
  ATH_CHECK( jets.isValid() );
  
  // set up decorators
  using JC = xAOD::JetContainer;
  SG::WriteDecorHandle<JC, ElementLink<JC>>  newlink(m_new_link, ctx);

  // decorate jets
  for (const auto jet: *jets) {
      ElementLink<JC> link(*jets, jet->index());
      newlink(*jet) = link;
  }

  return StatusCode::SUCCESS;
}
