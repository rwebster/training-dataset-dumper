/*
  Copyright (C) 2002-2045 CERN for the benefit of the ATLAS collaboration
*/
#include "TruthJetPrimaryVertexDecoratorAlg.h"
#include "StoreGate/WriteDecorHandle.h"

TruthJetPrimaryVertexDecoratorAlg::TruthJetPrimaryVertexDecoratorAlg(
  const std::string& name, ISvcLocator* pSvcLocator):
  AthReentrantAlgorithm(name, pSvcLocator)
{
}

StatusCode TruthJetPrimaryVertexDecoratorAlg::initialize() {
  ATH_CHECK(m_decPv.initialize());
  ATH_CHECK(m_events.initialize());
  return StatusCode::SUCCESS;
}

StatusCode TruthJetPrimaryVertexDecoratorAlg::execute(
  const EventContext& cxt) const
{
  SG::ReadHandle events(m_events, cxt);
  if (!events.isValid()) {
    ATH_MSG_ERROR("invalid event handle");
    return StatusCode::FAILURE;
  }
  if (events->size() != 1) {
    ATH_MSG_ERROR("number of truth events != 1");
    return StatusCode::FAILURE;
  }
  const xAOD::TruthVertex* pv = events->at(0)->signalProcessVertex();
  if (!pv) {
    ATH_MSG_ERROR("no pv found");
    return StatusCode::FAILURE;
  }
  SG::WriteDecorHandle<JC,float> jet_dec(m_decPv, cxt);
  for (const auto* jet: *jet_dec) {
    jet_dec(*jet) = pv->z();
  }
  return StatusCode::SUCCESS;
};

