#ifndef BTAG_JET_WRITER_CONFIG_HH
#define BTAG_JET_WRITER_CONFIG_HH

#include "VariablesByType.hh"

#include <map>
#include <string>
#include <vector>

struct BTagVariableMaps {
  std::map<std::string, std::string> replace_with_defaults_checks;
  std::map<std::string, std::string> rename;
};

struct BTagJetWriterBaseConfig {
  BTagJetWriterBaseConfig();
  std::string name;
  BTagVariableMaps variable_maps;
  std::string btagging_link;

  VariablesByCompression event;
  VariablesByType jet;
  VariablesByType btagging;
};

struct BTagJetWriterConfig: public BTagJetWriterBaseConfig {
  BTagJetWriterConfig();
  size_t n_jets_per_event;
  bool force_full_precision = false;
};

struct SubjetWriterConfig: public BTagJetWriterBaseConfig {
  SubjetWriterConfig();
  size_t n_subjets_to_save;
  bool write_kinematics_relative_to_parent;
};

#endif
