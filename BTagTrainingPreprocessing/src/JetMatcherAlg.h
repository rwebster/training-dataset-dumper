/*
  Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
*/

#ifndef TRIGGER_JET_MATCHER_ALG_H
#define TRIGGER_JET_MATCHER_ALG_H

#include "VariableMule.hh"

#include "xAODJet/JetContainer.h"
#include "AthenaBaseComps/AthReentrantAlgorithm.h"


class JetMatcherAlg: public AthReentrantAlgorithm
{
public:
  JetMatcherAlg(const std::string& name, ISvcLocator* pSvcLocator);

  // these are the functions inherited from Algorithm
  virtual StatusCode initialize () override;
  virtual StatusCode execute (const EventContext&) const override;
  virtual StatusCode finalize () override;
private:
  using JC = xAOD::JetContainer;
  using IPLV = std::vector<ElementLink<xAOD::IParticleContainer>>;
  VariableMule<float,JC> m_floats{NAN};
  VariableMule<int,JC> m_ints{-1};
  VariableMule<IPLV,JC> m_iparticles{{}};
  SG::ReadHandleKey<JC> m_targetJet {this, "targetJet", "", "target jet"};
  SG::ReadHandleKeyArray<JC> m_sourceJets;
  Gaudi::Property<std::string> m_dRKey {
    this, "dR", "deltaRToMatchedJet", "decorator for delta R to match"};
  Gaudi::Property<std::string> m_dPtKey {
    this, "dPt", "deltaPtToMatchedJet", "decorator for delta pt to match"};
  Gaudi::Property<float> m_ptPriorityWithDeltaR {
    this, "ptPriorityWithDeltaR", -1,
    "Give priority to higher pt truth jets, with some delta-R cut"
  };
  SG::WriteDecorHandleKey<JC> m_drDecorator;
  SG::WriteDecorHandleKey<JC> m_dPtDecorator;
  using JV = std::vector<const xAOD::Jet*>;
  std::function<const xAOD::Jet*(const xAOD::Jet*, const JV&)> m_jetSelector;
};

#endif
