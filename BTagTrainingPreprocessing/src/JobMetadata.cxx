#include "JobMetadata.hh"
#include "TruthTools.hh"
#include "SingleBTagConfig.hh"

#include "xAODEventInfo/EventInfo.h"

#include "nlohmann/json.hpp"

#include <fstream>
#include <sstream>



void writeJobMetadata(
  const std::vector<std::shared_ptr<TimingStats>>& timings,
  const std::string& output_file) {

  nlohmann::json metadata;

  metadata["tagger_timings"]["units"] = "milliseconds";
  for (const auto& timing : timings) {
    auto [n_jets, mean, variance] = timing->get_stats();
    float std_dev = std::sqrt(variance);
    metadata["tagger_timings"][timing->name]["n_jets"] = n_jets;
    metadata["tagger_timings"][timing->name]["mean"] = mean;
    metadata["tagger_timings"][timing->name]["standard_deviation"] = std_dev;
    metadata["tagger_timings"][timing->name]["standard_error"] = std_dev / std::sqrt(n_jets);
  }

  std::ofstream metastream(output_file);
  metastream << metadata.dump(2);
}

void addAttributeToHDF5(const std::filesystem::path& cfg_path, H5::Group& output_file) {
  // get merged config
  auto nlocfg = get_merged_json(cfg_path);

  addAttributeToHDF5(nlocfg.dump(), output_file);
}
void addAttributeToHDF5(const std::string& cfg, H5::Group& output_file) {
  //create attribute
  H5::StrType str_type(0, H5T_VARIABLE);
  H5::DataSpace att_space(H5S_SCALAR);
  H5::Attribute att = output_file.createAttribute( "config", str_type, att_space);
  att.write( str_type, cfg);
}
