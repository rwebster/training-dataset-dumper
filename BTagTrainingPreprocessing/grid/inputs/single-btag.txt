# Single b-tag training samples
# https://ftag.docs.cern.ch/software/samples/

# -------------------------------------------------------------------------------------------------
# New training samples: mc20d, mc23a and mc23c (https://its.cern.ch/jira/browse/ATLFTAGDPD-364)
# -------------------------------------------------------------------------------------------------
mc20_13TeV.601589.PhPy8EG_A14_ttbar_hdamp258p75_nonallhadron.deriv.DAOD_FTAG1.e8547_s3797_r13144_p5922
mc23_13p6TeV.601589.PhPy8EG_A14_ttbar_hdamp258p75_nonallhadron.deriv.DAOD_FTAG1.e8549_s4159_r14799_p5922
mc23_13p6TeV.601589.PhPy8EG_A14_ttbar_hdamp258p75_nonallhadron.deriv.DAOD_FTAG1.e8549_s4162_r14622_p5922

# ------------------------------------
# Default run 3 training samples: mc23a
# ------------------------------------
# ttbar
mc23_13p6TeV.601229.PhPy8EG_A14_ttbar_hdamp258p75_SingleLep.deriv.DAOD_FTAG1.e8514_s4162_r14622_p5922
mc23_13p6TeV.601230.PhPy8EG_A14_ttbar_hdamp258p75_dil.deriv.DAOD_FTAG1.e8514_s4162_r14622_p5922

# Z' extended
mc23_13p6TeV.800030.Py8EG_A14NNPDF23LO_flatpT_Zprime_Extended.deriv.DAOD_FTAG1.e8514_s4162_r14622_p5922

# ------------------------------
# mc21 run 3 training samples
# ------------------------------
# ttbar
#mc21_13p6TeV.601229.PhPy8EG_A14_ttbar_hdamp258p75_SingleLep.deriv.DAOD_FTAG1.e8453_s3873_r13829_p5627
#mc21_13p6TeV.601230.PhPy8EG_A14_ttbar_hdamp258p75_dil.deriv.DAOD_FTAG1.e8453_s3873_r13829_p5627
#mc21_13p6TeV.601589.PhPy8EG_A14_ttbar_hdamp258p75_nonallhadron.deriv.DAOD_FTAG1.e8485_s3873_r13829_p5627

# Z' extended
#mc21_13p6TeV.800030.Py8EG_A14NNPDF23LO_flatpT_Zprime_Extended.deriv.DAOD_FTAG1.e8464_s3873_r13829_p5627

# Z' extended radiatation damage off
#mc21_13p6TeV.800030.Py8EG_A14NNPDF23LO_flatpT_Zprime_Extended.deriv.DAOD_FTAG1.e8464_s3873_r14193_p5627	


# -------------------------------------
# Default run 2 training samples: mc20d
# -------------------------------------
# ttbar and Z' extended 
mc20_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.deriv.DAOD_FTAG1.e6337_s3681_r13144_p5922
mc20_13TeV.800030.Py8EG_A14NNPDF23LO_flatpT_Zprime_Extended.deriv.DAOD_FTAG1.e7954_s3681_r13144_p5922

# graviton
#mc20_13TeV.504648.aMCPy8EG_A14NNPDF23LO_Hjj_m3000w50.deriv.DAOD_FTAG1.e8418_s3681_r13144_p5770

# more VR specific samples (mc20a+d+e)
#mc20_13TeV.427080.Pythia8EvtGen_A14NNPDF23LO_flatpT_Zprime.deriv.DAOD_FTAG1.e5362_s3681_r13167_p5770
#mc20_13TeV.427080.Pythia8EvtGen_A14NNPDF23LO_flatpT_Zprime.deriv.DAOD_FTAG1.e5362_s3681_r13144_p5770
#mc20_13TeV.427080.Pythia8EvtGen_A14NNPDF23LO_flatpT_Zprime.deriv.DAOD_FTAG1.e5362_s3681_r13145_p5770

# alternative generators
#mc20_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.deriv.DAOD_FTAG1.e6337_a899_r13144_p5770
#mc20_13TeV.500568.MGPy8EG_NNPDF23ME_A14_ZPrime.deriv.DAOD_FTAG1.e7954_s3126_r13144_p5770
#mc20_13TeV.500567.MGH7EG_NNPDF23ME_Zprime.deriv.DAOD_FTAG1.e7954_s3681_r13144_p5770
#mc20_13TeV.800030.Py8EG_A14NNPDF23LO_flatpT_Zprime_Extended.deriv.DAOD_FTAG1.e7954_a899_r13144_p5770

#dijet and V+jets samples
#mc20_13TeV.364702.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ2WithSW.deriv.DAOD_FTAG1.e7142_s3681_r13144_p5770
#mc20_13TeV.700321.Sh_2211_Zee_maxHTpTV2_CFilterBVeto.deriv.DAOD_FTAG1.e8351_s3681_r13144_p5770
#mc20_13TeV.700324.Sh_2211_Zmumu_maxHTpTV2_CFilterBVeto.deriv.DAOD_FTAG1.e8351_s3681_r13144_p5770
#mc20_13TeV.700335.Sh_2211_Znunu_pTV2_BFilter.deriv.DAOD_FTAG1.e8351_s3681_r13144_p5770
#mc20_13TeV.700323.Sh_2211_Zmumu_maxHTpTV2_BFilter.deriv.DAOD_FTAG1.e8351_s3681_r13144_p5770
#mc20_13TeV.700325.Sh_2211_Zmumu_maxHTpTV2_CVetoBVeto.deriv.DAOD_FTAG1.e8351_s3681_r13144_p5770
#mc20_13TeV.700337.Sh_2211_Znunu_pTV2_CVetoBVeto.deriv.DAOD_FTAG1.e8351_s3681_r13144_p5770
#mc20_13TeV.700332.Sh_2211_Ztautau_HH_maxHTpTV2_BFilter.deriv.DAOD_FTAG1.e8351_s3681_r13144_p5770
#mc20_13TeV.700334.Sh_2211_Ztautau_HH_maxHTpTV2_CVetoBVeto.deriv.DAOD_FTAG1.e8351_s3681_r13144_p5770
#mc20_13TeV.700333.Sh_2211_Ztautau_HH_maxHTpTV2_CFilterBVeto.deriv.DAOD_FTAG1.e8351_s3681_r13144_p5770
#mc20_13TeV.700322.Sh_2211_Zee_maxHTpTV2_CVetoBVeto.deriv.DAOD_FTAG1.e8351_s3681_r13144_p5770
#mc20_13TeV.700320.Sh_2211_Zee_maxHTpTV2_BFilter.deriv.DAOD_FTAG1.e8351_s3681_r13144_p5770
#mc20_13TeV.700336.Sh_2211_Znunu_pTV2_CFilterBVeto.deriv.DAOD_FTAG1.e8351_s3681_r13144_p5770
#mc20_13TeV.364705.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ5WithSW.deriv.DAOD_FTAG1.e7142_s3681_r13144_p5770
#mc20_13TeV.364703.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ3WithSW.deriv.DAOD_FTAG1.e7142_s3681_r13144_p5770
#mc20_13TeV.364704.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ4WithSW.deriv.DAOD_FTAG1.e7142_s3681_r13144_p5770
