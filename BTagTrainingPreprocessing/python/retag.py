"""
Here we construct a retagging sequence that takes a few variables
Such as :
  the input track, jet collections
  list of tracking systematics to run over
  option to merge LRT and standard tracks
This allows the users to perform retagging with more flexibilities
"""

from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator
from BTagTrainingPreprocessing.trackUtil import LRTMerger, applyTrackSys

from JetTagCalibration.JetTagCalibConfig import JetTagCalibCfg
from BTagging.BTagTrackAugmenterAlgConfig import BTagTrackAugmenterAlgCfg
from BTagging.BTagConfig import RetagRenameInputContainerCfg, GetTaggerTrainingMap
from BTagging.BTagConfig import BTagAlgsCfg
from AthenaConfiguration.ComponentFactory import CompFactory

def retagging(
    cfgFlags,
    merge,
    track_collection="InDetTrackParticles",
    jet_collection="",
    sys_list=[],
    decorate_track_truth=False,
):

    acc = ComponentAccumulator()

    pvCol = "PrimaryVertices"
    jet_collection = jet_collection    
    # When the jet systematic is applied, the input jets will become AntiKt4EMPFlowJetsSys. 
    jet_collection_nosuffix = jet_collection.replace("Jets", "").rstrip("Sys")
    muon_collection = "Muons" if track_collection == "InDetTrackParticles" else None
    track_collection = track_collection

    # get list of taggers to run
    trainingMap = GetTaggerTrainingMap(cfgFlags, jet_collection_nosuffix)

    if sys_list:
        acc.merge(applyTrackSys(sys_list, track_collection, jet_collection))
        track_collection = "InDetTrackParticles_Sys"

    # The decoration on tracks needs to happen before the track merging step
    # Decorate the standard tracks first
    acc.merge(
        BTagTrackAugmenterAlgCfg(
            cfgFlags,
            TrackCollection=track_collection,
            PrimaryVertexCollectionName=pvCol,
        )
    )
    if decorate_track_truth:
        from InDetTrackSystematicsTools.InDetTrackSystematicsToolsConfig import InDetTrackTruthOriginToolCfg
        trackTruthOriginTool = acc.popToolsAndMerge(InDetTrackTruthOriginToolCfg(cfgFlags))
        acc.addEventAlgo(CompFactory.FlavorTagDiscriminants.TruthParticleDecoratorAlg(
            'TruthParticleDecoratorAlg',
            trackTruthOriginTool=trackTruthOriginTool
        ))
        acc.addEventAlgo(CompFactory.FlavorTagDiscriminants.TrackTruthDecoratorAlg(
            'TrackTruthDecoratorAlg',
            trackContainer=track_collection,
            trackTruthOriginTool=trackTruthOriginTool
        ))

    if merge:
        # Decorate the LRT tracks
        acc.merge(
            BTagTrackAugmenterAlgCfg(
                cfgFlags,
                TrackCollection="InDetLargeD0TrackParticles",
                PrimaryVertexCollectionName=pvCol,
            )
        )

        # merge the LRT and standard tracks into a single view collection
        acc.merge(LRTMerger())
        track_collection = "InDetWithLRTTrackParticles"

    # rename containers to avoid clashes
    addRenameMaps=['xAOD::TrackParticleAuxContainer#' + track_collection + '.btagIp_invalidIp->' + track_collection + '.btagIp_invalidIp_' + '_retag']
    acc.merge(RetagRenameInputContainerCfg("_retag", jet_collection_nosuffix, tracksKey=track_collection, addRenameMaps=addRenameMaps))
    acc.merge(JetTagCalibCfg(cfgFlags))

    # add the main b-tagging config
    acc.merge(
        BTagAlgsCfg(
            cfgFlags,
            JetCollection=jet_collection,
            nnList=trainingMap,
            trackCollection=track_collection,
            muons=muon_collection,
            renameTrackJets = True,
            AddedJetSuffix = ''
    ))

    return acc