# Advanced Use Cases


### Running with CA

A very simple `ComponentAccumulator`-based script to run it is provided in [`ca-dump-single-btag`]({{repo_url}}-/blob/main/BTagTrainingPreprocessing/bin/ca-dump-single-btag).
You can test it with

```bash
test-dumper ca
```

There are a number of other scripts matching `ca-*`: these are based around `ComponentAccumulator` Athena configuration.
Use the built in help (e.g. `ca-dump-single-btag -h`) to learn more about them.

### Running Decoration Algorithms from Athena

As discussed in the [contributing guidelines](contributing.md#adding-more-outputs),
it is preferred to add decoration algorithms directly to Athena, rather than in this package.
The Athena algorithms can then be scheduled to run before a dumping job, which is useful when the 
algorithm has not already been run in the derivations.

Scheduling an Athena algorithm amounts to adding a few lines to the CA script.
The lines you need to add are shown in the `# add an algorithm` block in the example below.

```python
def run():
    # boilerplate stuff here
    ...
    
    # get the top level CA
    ca = dumper.getMainConfig(cfgFlags, args)

    # add an algorithm
    sub = ComponentAccumulator()
    sub.addEventAlgo(CompFactory.AlgPackage.AlgName('MyAlgName'))
    ca.merge(sub)

    # add the dumper
    ca.merge(dumper.getDumperConfig(args))
```

A complete example can be found in the 
[`ca-dump-softe`]({{repo_url}}-/blob/main/BTagTrainingPreprocessing/bin/ca-dump-softe)
script. Which schedules both the `SingleBTagElectronAlg` from this package and the 
`ElectronDecoratorAlg` from Athena.


### Retagging with locally modified Athena packages

In some cases, you may need to modify a part of the flavour-tagging full chain. For instance, you may want to tune one of the secondary vertexing algorithms to identify its best working point after some change occurred in Athena. To do that, you need a tool that allows you to re-run flavour tagging on your sample [importing the changes you made on Athena packages](contributing.md#editing-athena-packages).

[`ca-dump-retag`]({{repo_url}}-/blob/main/BTagTrainingPreprocessing/bin/ca-dump-retag), the script that implements retagging, is such tool.

To use it, you need to setup your environment with `setup/athena.sh`.

The code runs as follows:
```bash
ca-dump-retag -c <path to configuration file> <paths to xAOD(s)>
```

To run on the grid, use:
```bash
grid-submit retag
```

### Run on heavy ion xAODs

You need to run under athanalysis (use `setup/athanalysis.sh`).

Since HI xAOD are still produced under R21 we can dump using:

```bash
ca-dump-upgrade-HI -c <path to configuration file> <paths to xAOD(s)>
```

The configuration file should be `upgrade-HI.json`, which will use `pflow-variables.json`. Flag `do_heavions` must be set to true in `upgrade-HI.json`.

To run on the grid use:
```bash
grid-submit upgrade-HI
```

### Run on Upgrade samples

For Upgrade DAODs produced in release 24, the default [`ca-dump-single-btag`]({{repo_url}}-/blob/main/BTagTrainingPreprocessing/bin/ca-dump-single-btag) steering should be used.


### Run on trigger samples

You can make trigger training datasets in Athena with

```bash
ca-dump-trigger-pflow -c <dumper>/configs/trigger.json xAOD.root
```

This is a _much_ heavier job than the standard ones, because it has to
load in a number of dependencies for track extrapolation. As with the
other scripts, see the `-h` for more options.

You can use `grid-submit trigger` to submit these jobs to the grid.

### Working with nightlies

If you're on the bleeding edge of ATLAS software, there may not be a stable release that supports a new feature you need.
The scripts `setup/athena-latest.sh` and `setup/analysisbase-latest.sh` will set up the latest nightly build.

Working with nightlies comes with a few caveats:

- We make no promises that the code will work with them! Our continuous integration tests all updates in the tagged releases for both AnalysisBase and AthAnalysis based projects. This is not extended to nightlies.
- Nightlies will disappear after a few weeks by default. Because of this, **producing larger datasets** based on a nightly is **strongly discoursged:** no one will be able to reproduce your work when the nightly is deleted. If you have to do this, you should open a nightly preservation request, for example as in [ATLINFR-4697](https://its.cern.ch/jira/browse/ATLINFR-4697).
